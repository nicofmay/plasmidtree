/*
 * Copyright (C) 2015 Tim Vaughan <tgvaughan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package plasmidTree.util;

import plasmidTree.ConversionGraphPlasmid;
import plasmidTree.Plasmid;
import beast.core.CalculationNode;
import beast.core.Description;
import beast.core.Input;
import beast.core.Input.Validate;
import beast.core.Loggable;

import java.io.PrintStream;

/**
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
@Description("Logs ACGs with root-connecting conversions removed.")
public class ACGLoggerPlasmid extends CalculationNode implements Loggable {

    public Input<ConversionGraphPlasmid> acgInput = new Input<>(
            "acg", "Conversion graph whose trimmed representation to log.",
            Validate.REQUIRED);
    
    public Input<Plasmid> plasmidInput = new Input<>(
            "plasmid", "Plasmid for which to log conversion graph.",
            Validate.REQUIRED);

    @Override
    public void initAndValidate() { }
    
    @Override
    public void init(PrintStream out) {
       acgInput.get().init(out);
    }

    @Override
    public void log(long nSample, PrintStream out) {
        ConversionGraphPlasmid arg = acgInput.get();
        Plasmid locus=plasmidInput.get();
        out.print("tree STATE_" + nSample + " = ");
        if(locus.isPlasmid) out.print(arg.getExtendedNewickPlasmid(locus));
        else out.print(arg.getExtendedNewickPlasmid());
    }

    @Override
    public void close(PrintStream out) {
        acgInput.get().close(out);
    }
    
}
