/*
 * Copyright (C) 2017 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package plasmidTree.model;

//import bacter.CFEventList;
import plasmidTree.CFEventListPlasmid;
//import bacter.Conversion;
//import bacter.ConversionGraph;
import plasmidTree.ConversionGraphPlasmid;
//import bacter.Locus;
import plasmidTree.Plasmid;
import plasmidTree.ConversionPlasmid;
import beast.core.Description;
import beast.core.Input;
import beast.core.State;
import beast.core.parameter.RealParameter;
import beast.evolution.tree.Node;
import beast.evolution.tree.TreeDistribution;
import beast.evolution.tree.coalescent.PopulationFunction;

//import org.apache.commons.math.MathException;
//import org.apache.commons.math.distribution.PoissonDistributionImpl;

//import bacter.CFEventList;
//import bacter.Conversion;

import java.util.ArrayList;
import java.util.Collections;
//import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author Nicola De Maio
 */
@Description("Appoximation to the coalescent with gene conversion.")
public class ACGCoalescentPlasmid extends TreeDistribution {

    public Input<PopulationFunction> popFuncInput = new Input<>(
            "populationModel", "Population model.", Input.Validate.REQUIRED);
    
    public Input<RealParameter> rhoInput = new Input<>("rho",
            "Recombination/plasmid gain rate parameter.", Input.Validate.REQUIRED);
    
    public Input<RealParameter> lInput = new Input<>("l",
            "plasmid loss rate parameter.", Input.Validate.REQUIRED);
    
    public Input<Boolean> useCoalInput = new Input<>("useCoal",
            "use coalescent distribution for arrival times of recombinant edges.", true);
    
    //public Input<RealParameter> deltaInput = new Input<>("delta", "Tract length parameter.", Input.Validate.REQUIRED);
    
    //public Input<RealParameter> deltaInput = new Input<>("delta", "Rate of plasmid loss.", Input.Validate.REQUIRED);

    //public Input<Integer> lowerCCBoundInput = new Input<>("lowerConvCountBound",
    //        "Lower bound on conversion count.", 0);

    //public Input<Integer> upperCCBoundInput = new Input<>("upperConvCountBound",
    //        "Upper bound on conversion count.", Integer.MAX_VALUE);

    // WARNING: The presence of the following input is a hack. This value is
    // not directly used by BEAST, but is instead copied via a custom BEAUti
    // connector to the similarly-named input to ConversionGraph.
    //
    // The reason for this ugliness is that BEAUti does not allow users to
    // alter inputs to Trees.  (The Tree input editor is used to set tip dates.)
    public Input<Boolean> wholeLocusConversionsInput = new Input<>(
            "wholeLocusConversionsOnly",
            "Only allow whole loci to be converted.", false);

    ConversionGraphPlasmid acg;
    PopulationFunction popFunc;
    Boolean useCoal;
    Double rho, l;
    

    public ACGCoalescentPlasmid() {
        treeInput.setRule(Input.Validate.REQUIRED);
    }
    
    @Override
    public void initAndValidate() {
        if (!(treeInput.get() instanceof ConversionGraphPlasmid))
            throw new IllegalArgumentException("Tree input to ACGCoalescent " +
                    "must specify a ConversionGraph.");

        if (treeIntervalsInput.get() != null)
            throw new IllegalArgumentException("ACGCoalescent does not accept " +
                    "the treeIntervals input.");

        acg = (ConversionGraphPlasmid)treeInput.get();
        popFunc = popFuncInput.get();
        useCoal  = useCoalInput.get();
        rho = rhoInput.get().getValue();
        l = lInput.get().getValue();
    }
    
    @Override
    public double calculateLogP() {

        // Check whether conversion count exceeds bounds.
        //if (acg.getTotalConvCount()<lowerCCBoundInput.get()
        //        || acg.getTotalConvCount()>upperCCBoundInput.get())
        //    return Double.NEGATIVE_INFINITY;

    	//System.out.println("LogP before CF: "+logP+" rho "+rho+" l "+l+"\n");
        logP = calculateClonalFrameLogP();
        //System.out.println("LogP after CF: "+logP+"\n");
        
        //NICOLA: add the likelihood of the gain-loss history for each plasmid
        rho = rhoInput.get().getValue();
        l = lInput.get().getValue();
        //System.out.println("Rho: "+rho+" l "+l+"\n");
        for (Plasmid locus : acg.getLoci()){
        	if (locus.isPlasmid){
        		//System.out.println("LogP before one GL: "+logP);
                logP += calculateLossGainLogP(locus);
                //System.out.println("LogP after one gain-losses for plasmid "+locus.getID()+" : "+logP+"\n");
        	}
        }
        
        //double poissonMean = rhoInput.get().getValue()
        //        *acg.getClonalFrameLength()
        //        *(acg.getTotalSequenceLength()
        //        +acg.getLoci().size()*(deltaInput.get().getValue()-1.0));

        // Probability of conversion count:
//        if (poissonMean>0.0) {
//            logP += -poissonMean + acg.getTotalConvCount()*Math.log(poissonMean);
//            //      - GammaFunction.lnGamma(acg.getConvCount()+1);
//        } else {
//            if (acg.getTotalConvCount()>0)
//                logP = Double.NEGATIVE_INFINITY;
//        }
        

        for (Plasmid locus : acg.getLoci()){
        	//System.out.println("Plasmid: "+locus.getID());
        	if (locus.isPlasmid){
        		List<ConversionPlasmid> pgl = new ArrayList<>();
            	pgl=acg.getConversions(locus);
            	//System.out.println(pgl.size()+" conversions \n");
            	for (ConversionPlasmid conv : pgl) {    
            		//System.out.println("LogP before adding a conversion : "+logP);
            		logP += calculateConversionLogP(conv);
            		//System.out.println("LogP after adding a conversion: "+logP);
            	}	
        	}
        	//System.out.println("LogP after adding all conversions for locus "+locus.getID()+" : "+logP+"\n");
        }
        
        // This N! takes into account the permutation invariance of
        // the individual conversions, and cancels with the N! in the
        // denominator of the Poissonian above.
        // logP += GammaFunction.lnGamma(acg.getConvCount() + 1);

//        if (lowerCCBoundInput.get()>0 || upperCCBoundInput.get()<Integer.MAX_VALUE) {
//            try {
//                logP -= new PoissonDistributionImpl(poissonMean)
//                        .cumulativeProbability(
//                                lowerCCBoundInput.get(),
//                                upperCCBoundInput.get());
//            } catch (MathException e) {
//                throw new RuntimeException("Error computing modification to ARG " +
//                        "prior density required by conversion number constraint.");
//            }
//        }
        //System.out.println("LogP after all coalescent things "+logP+"\n");
        //if(Math.abs(rho-l)>1e-7) System.exit(0);
        //if(Math.abs(rho)>0.6) System.exit(0);
        return logP;
    }
    
    
    
    
    public double calculateLossGainLogP(Plasmid locus) {
    	List<ConversionPlasmid> pgl = acg.getConversions(locus); //new ArrayList<>();
		//pgl=acg.getConversions(locus);
		//create maps with all gene gains time and coversion coalescent times for all branches (represented by the bottom nodes).
		Map<Node,List<Double>> gains = new HashMap<Node,List<Double>>();
		Map<Node,List<Double>> coals = new HashMap<Node,List<Double>>();
		for (ConversionPlasmid conv : pgl) {
			if(!gains.containsKey(conv.getNode1())) {
				gains.put(conv.getNode1(), new ArrayList<Double>());
			}
			gains.get(conv.getNode1()).add(conv.getHeight1());
			if(!coals.containsKey(conv.getNode2())) {
				coals.put(conv.getNode2(), new ArrayList<Double>());
			}
			coals.get(conv.getNode2()).add(conv.getHeight2());
		}
		for (Node n:gains.keySet()) {
			Collections.sort(gains.get(n));
		}
		for (Node n:coals.keySet()) {
			Collections.sort(coals.get(n));
		}
		Node root = treeInput.get().getRoot();
		//equilibrium frequencies for root
		double pi0=l/(l+rho);
		double pi1=rho/(rho+l);
		//System.out.println("pi0: "+pi0+" pi1: "+pi1+" gains "+gains.size()+" coals "+coals.size()+" calling iterativeLkGainLoss\n");
		//call an iterative function on children, returning a pair of values: likelihood of 0 state and likelihood of 1 state (plasmid absent/present)
		//at the considered node. Final likelihood is that sum of these values at the root weighted by the equilibrium frequency of presence and absence.
		ProbCouple lk=iterativeLkGainLoss(root,gains,coals,locus);
		//System.out.println("Contribution of plasmid "+locus.getID()+" : "+Math.log(lk.average(pi0,pi1))+"\n");
		return Math.log(lk.average(pi0,pi1));
    }
    
    
    //Nicola: function that iteratively calculates the likelihood of gains and losses (losses are integrated over) 
    public ProbCouple iterativeLkGainLoss(Node n, Map<Node,List<Double>> gains, Map<Node,List<Double>> coals, Plasmid locus){
    		//System.out.println("Start iterativeGL, node: "+n.getID()+" height "+n.getHeight()+"\n");
    		ProbCouple PC = new ProbCouple();
    		if (n.isLeaf()) {
    			//System.out.println("Inside if");
    			if(locus.getAlignment().getTaxonIndex(n.getID())!=-1) {// .contains(n.getID())
    				PC.p1=1.0;
    				PC.p0=0.0;
    			}else {
    				PC.p1=0.0;
    				PC.p0=1.0;
    			}
    			//System.out.println("Leaf Retunrning "+PC.p0+" "+PC.p1+"\n");
    			return PC;
    		}
    		PC.p0=1.0;
    		PC.p1=1.0;
    		//System.out.println("before for");
    		//List<ProbCouple> childrenPC = new ArrayList<ProbCouple>();
    		for (Node c : n.getChildren()) {
    			//System.out.println("Start for child "+c.getID()+" height "+c.getHeight()+"\n");
    			//iterate over the likelihoods of the children
    			ProbCouple cPC=iterativeLkGainLoss(c, gains, coals, locus);
    			//System.out.println("Returning to caller node: "+n.getID()+" height "+n.getHeight()+" with probs "+cPC.p0+" "+cPC.p1+"\n");
    			List<Double> cGains=gains.get(c);
    			//System.out.println("gains");
    			List<Double> cCoals=coals.get(c);
    			//System.out.println("coals");
    			double time =c.getHeight();
    			int cIndex=0;
    			int gIndex=0;
    			double nextTime=n.getHeight();
    			//iterate over the events on a branch
    			//System.out.println("Before second for");
    			int cSize=0;
    			if (cCoals!=null) cSize = cCoals.size();
    			int gSize=0;
    			if (cGains!=null) gSize = cGains.size();
    			for(int i=0; i<gSize+cSize+1;i++) {
    				//System.out.println("Start second for, i "+i+" , current cPC: "+cPC.p0+" "+cPC.p1+"\n");
    				nextTime=n.getHeight();
    				double p10;
					if (Math.abs(l - rho) < 1e-7) p10=l*(nextTime-time)*Math.exp(-l*(nextTime-time));
					else p10= l*(Math.exp(-rho*(nextTime-time))-Math.exp(-l*(nextTime-time)))/(l-rho);
					//System.out.println("Updated p10 "+p10+", p11 "+Math.exp(-l*(nextTime-time))+" cPC "+cPC.p0+" "+cPC.p1+"\n");
    				if (gIndex<gSize && cGains.get(gIndex)<=nextTime && ( cIndex>=cSize || cGains.get(gIndex)<cCoals.get(cIndex))) {
    					//next event is a gain
    					nextTime=cGains.get(gIndex);
    					gIndex+=1;
    					//System.out.println("l "+l+" rho "+rho+", (nextTime-time) "+(nextTime-time)+" -l*(nextTime-time) "+-l*(nextTime-time)+" Math.exp(-l*(nextTime-time)) "+Math.exp(-l*(nextTime-time))+" (Math.abs(l - rho) < 1e-7) "+(Math.abs(l - rho) < 1e-7)+"\n");
    					//System.out.println("Updating cPc with p10 "+p10+", p11 "+Math.exp(-l*(nextTime-time))+" cPC "+cPC.p0+" "+cPC.p1+"\n");
    					cPC=cPC.likelihoodUpdate(0.0, 0.0, p10, Math.exp(-l*(nextTime-time)));
    					//System.out.println("Updated p10 "+p10+", p11 "+Math.exp(-l*(nextTime-time))+" cPC "+cPC.p0+" "+cPC.p1+"\n");
    					cPC.p0=cPC.p1*rho;
    					//System.out.println(" cPC "+cPC.p0+" "+cPC.p1+"\n");
    					cPC.p1=0.0;
    					//System.out.println("First case (gain) , times "+time+" "+nextTime+", cPC has become: "+cPC.p0+" "+cPC.p1+"\n");
    				}else if (cIndex<cSize && cCoals.get(cIndex)<=nextTime && ( gIndex>=gSize || cGains.get(gIndex)>=cCoals.get(cIndex))) {
    					//next event is a coal of a gain
    					nextTime=cCoals.get(cIndex);
    					cIndex+=1;
    					//System.out.println("p10 "+p10+", p11 "+Math.exp(-l*(nextTime-time))+"\n");
    					cPC=cPC.likelihoodUpdate(0.0, 0.0, p10, Math.exp(-l*(nextTime-time)));
    					//System.out.println("Second case (coal) , times "+time+" "+nextTime+", cPC has become: "+cPC.p0+" "+cPC.p1+"\n");
    				}else {
    					//next event is the end of the branch
    					//System.out.println("p00 "+Math.exp(-rho*(nextTime-time))+" , p10 "+p10+", p11 "+Math.exp(-l*(nextTime-time))+"\n");
    					cPC=cPC.likelihoodUpdate(Math.exp(-rho*(nextTime-time)), 0.0, p10, Math.exp(-l*(nextTime-time)));
    					//System.out.println("Third case (end of branch), times "+time+" "+nextTime+" , cPC has become: "+cPC.p0+" "+cPC.p1+"\n");
    				}
    				time=nextTime;
    				//System.out.println("At time "+time+" child has probs "+cPC.p0+" "+cPC.p1+"\n");
    				//System.out.println("End second for");
    			}
    			//add contribution of likelihood of the child
    			//System.out.println("Adding contribution of the child "+cPC.p0+" "+cPC.p1+"\n");
    			PC.p0=PC.p0*cPC.p0;
    			PC.p1=PC.p1*cPC.p1;
    			//System.out.println("Parent probs is now: "+PC.p0+" "+PC.p1+"\n");
    			//childrenPC.add(cPC);
    			//System.out.println("End for");
    		}
    		return PC;
    }
    
    public class ProbCouple {
    		public double p0;
    		public double p1;
    		public ProbCouple(){};
    		public ProbCouple(double p0, double p1){
    			this.p0 = p0;
    			this.p1 = p1;
    		}
    		public double average(double p0, double p1) {
    			return p0*this.p0 + p1*this.p1;
    		}
    		public ProbCouple likelihoodUpdate(double p00, double p01, double p10, double p11) {
    			ProbCouple PCnew=new ProbCouple();
    			PCnew.p0=p0*p00 + p1*p01;//0, 0, 0.08436874533919422  0.9116038176254807 cPC 1.0 0.0
    			//0+0
    			PCnew.p1=p1*p11 + p0*p10;//0+0.08
    			return PCnew;
    		}
    }
    
    

    /**
     * Compute probability of clonal frame under coalescent.
     * 
     * @return log(P)
     */
    public double calculateClonalFrameLogP() {
        
        List<CFEventListPlasmid.Event> events = acg.getCFEvents();
        
        double thisLogP = 0.0;
        
        for (int i=0; i<events.size()-1; i++) {
            double timeA = events.get(i).getHeight();
            double timeB = events.get(i+1).getHeight();

            double intervalArea = popFunc.getIntegral(timeA, timeB);
            int k = events.get(i).getLineageCount();
            thisLogP += -0.5*k*(k-1)*intervalArea;
            
            if (events.get(i+1).getType()==CFEventListPlasmid.EventType.COALESCENCE)
                thisLogP += Math.log(1.0/popFunc.getPopSize(timeB));
        }
        
        return thisLogP;
    }
    
    
    
    /**
     * Compute probability of recombinant edges under conditional coalescent.
     * @param conv conversion with which edge is associated
     * @return log(P)
     */
    public double calculateConversionLogP(ConversionPlasmid conv) {

        double thisLogP = 0.0;

        List<CFEventListPlasmid.Event> events = acg.getCFEvents();

        // Probability density of location of recombinant edge start
        //thisLogP += Math.log(1.0/acg.getClonalFrameLength());
        
        if (!useCoal) return thisLogP;

        // Identify interval containing the start of the recombinant edge
        int startIdx = 0;
        while (events.get(startIdx+1).getHeight() < conv.getHeight1())
            startIdx += 1;

        for (int i=startIdx; i<events.size() && events.get(i).getHeight()<conv.getHeight2(); i++) {

            double timeA = Math.max(events.get(i).getHeight(), conv.getHeight1());

            double timeB;
            if (i<events.size()-1)
                timeB = Math.min(conv.getHeight2(), events.get(i+1).getHeight());
            else
                timeB = conv.getHeight2();

            double intervalArea = popFunc.getIntegral(timeA, timeB);
            thisLogP += -events.get(i).getLineageCount()*intervalArea;
        }

        // Probability of single coalescence event
        thisLogP += Math.log(1.0/popFunc.getPopSize(conv.getHeight2()));

        // Probability of start site:
        //if (conv.getStartSite()==0) {
        //    thisLogP += Math.log(deltaInput.get().getValue()
        //            / (acg.getLoci().size() * (deltaInput.get().getValue() - 1)
        //            + acg.getTotalSequenceLength()));
        //} else {
        //    if (!acg.wholeLocusModeOn())
        //        thisLogP += Math.log(
        //                1.0 / (acg.getLoci().size() * (deltaInput.get().getValue() - 1)
        //                        + acg.getTotalSequenceLength()));
        //    else
        //        return Double.NEGATIVE_INFINITY;
        //}

        // Probability of end site:
//        if (conv.getEndSite() == conv.getLocus().getSiteCount()-1) {
//            thisLogP += (conv.getLocus().getSiteCount()-1-conv.getStartSite())
//                    *Math.log(1.0 - 1.0/deltaInput.get().getValue());
//        } else {
//            if  (!acg.wholeLocusModeOn())
//                thisLogP += (conv.getEndSite()-conv.getStartSite())
//                        *Math.log(1.0 - 1.0/deltaInput.get().getValue())
//                        -Math.log(deltaInput.get().getValue());
//            else
//                return Double.NEGATIVE_INFINITY;
//        }

        return thisLogP;
    }
    
    
    
    
    
    
//    /**
//     * Compute probability of the plasmid gain/loss history conditional on the clonal frame.
//     * @param pgl list of plasmid gains and losses affecting the plasmid
//     * @param locus the considered plasmid
//     * @return log(P)
//     */
//    public double calculateConversionLogP(List<ConversionPlasmid> pgl, Plasmid locus) {
//
//        double thisLogP = 0.0;
//        
//        List<Node> lineages =new ArrayList<>();//active lineages while moving up the tree
//        List<Boolean> present =new ArrayList<>();//is the current plasmid present at the considered node (base of the considered branch)?
//
//        List<CFEventListPlasmid.Event> events = acg.getCFEvents();
//        
//        for (int i=0; i<events.size()-1; i++) {
//            //double timeA = events.get(i).getHeight();
//            //double timeB = events.get(i+1).getHeight();
//            Node n =events.get(i).getNode();
//            double timeDown =n.getHeight();
//            Node p=n.getParent();
//            double timeUp;
//            int index;
//            if (p!=null){ timeUp =n.getParent().getHeight();
//            }else{
//            	timeUp=Double.POSITIVE_INFINITY;
//            }
//            if (timeUp<timeDown){
//            	System.out.println("node-parent node times error.");
//            	System.exit(0);
//            }
//            if (events.get(i).getType()==CFEventListPlasmid.EventType.SAMPLE){
//            	lineages.add(n);
//            	present.add(locus.getAlignment().contains(n.getID())); //NICOLA: check that the node ID corresponds to the taxon ID
//            	index=present.size()-1;
//            }else{
//            	Node n1=n.getLeft();
//            	Node n2=n.getRight();
//            	int index1=lineages.indexOf(n1);
//            	int index2=lineages.indexOf(n2);
//            	boolean b1=present.get(index1);
//            	boolean b2=present.get(index2);
//            	if (b1!=b2){
//            		System.out.println("Inconsistent children for gains-losses.");
//        			return Double.NEGATIVE_INFINITY;
//            	}
//            	boolean currentlyPresent=b1;
//            	double currentHeight=timeDown;
//            	int indexMax, indexMin;
//            	if (index1>index2){
//            		indexMax=index1;
//            		indexMin=index2;
//            	}else{
//            		indexMax=index2;
//            		indexMin=index1;
//            	}
//            	lineages.remove(indexMax);
//            	present.remove(indexMax);
//            	lineages.set(indexMin, n);
//            	present.set(indexMin, currentlyPresent);
//            	index=indexMin;
//            }
//            List<ConversionPlasmid> nodeGains = new ArrayList<>();
//        	for (ConversionPlasmid gain : pgl){
//        		if (gain.getNode1().equals(n)){
//        			nodeGains.add(gain);
//        		}
//        	}
//        	Collections.sort(nodeGains, new Comparator<ConversionPlasmid>() { //NICOLA: check that list is sorted from smaller to bigger
//        	    @Override
//        	    public int compare(ConversionPlasmid o1, ConversionPlasmid o2) {
//        	        return ((Double)o1.getHeight1()).compareTo((Double)o2.getHeight1());
//        	    }
//        	});
//        	boolean currentlyPresent=present.get(present.size()-1);
//        	double currentHeight=timeDown;
//        	for (ConversionPlasmid gain : nodeGains){
//        		double gainTime=gain.getHeight1();
//        		if (gainTime<timeDown || gainTime>timeUp) {
//        			System.out.println("Gain time out of branch limits.");
//        			return Double.NEGATIVE_INFINITY;
//        		}
//        		if (gain.getNode2()==null){//plasmid loss
//        			if (currentlyPresent==true){
//        				System.out.println("Inconsistent order of gains-losses (loss).");
//            			return Double.NEGATIVE_INFINITY;
//        			}
//        			currentlyPresent=true;
//        			thisLogP+=(-rhoInput.get().getValue()*(gain.getHeight1()-currentHeight))+Math.log(deltaInput.get().getValue());
//        			currentHeight=gain.getHeight1();
//        		}else{
//        			if (currentlyPresent==false){
//        				System.out.println("Inconsistent order of gains-losses (gain).");
//            			return Double.NEGATIVE_INFINITY;
//        			}
//        			currentlyPresent=false;
//        			thisLogP+=(-deltaInput.get().getValue()*(gain.getHeight1()-currentHeight))+Math.log(rhoInput.get().getValue());
//        			currentHeight=gain.getHeight1();
//        		}
//        	}
//            present.set(index, currentlyPresent);
//        }
//        
//        //NICOLA: For now there is no probability associated with the coalescent events of recombinant edges. One option could be to use the 
//        //standard coalescent, allowing coalescent only with edges where the plasmid is present. However, It would also makes sense to allow a different 
//        //population size (as only part of the bacteria will have such plasmid) or a structured coalescent or a multispecies coalescent model given 
//        //that plasmids might be passed across different species/strains.
//
////        // Probability density of location of recombinant edge start
////        thisLogP += Math.log(1.0/acg.getClonalFrameLength());
////
////        // Identify interval containing the start of the recombinant edge
////        int startIdx = 0;
////        while (events.get(startIdx+1).getHeight() < conv.getHeight1())
////            startIdx += 1;
////
////        for (int i=startIdx; i<events.size() && events.get(i).getHeight()<conv.getHeight2(); i++) {
////
////            double timeA = Math.max(events.get(i).getHeight(), conv.getHeight1());
////
////            double timeB;
////            if (i<events.size()-1)
////                timeB = Math.min(conv.getHeight2(), events.get(i+1).getHeight());
////            else
////                timeB = conv.getHeight2();
////
////            double intervalArea = popFunc.getIntegral(timeA, timeB);
////            thisLogP += -events.get(i).getLineageCount()*intervalArea;
////        }
////
////        // Probability of single coalescence event
////        thisLogP += Math.log(1.0/popFunc.getPopSize(conv.getHeight2()));
//
//        return thisLogP;
//    }

    @Override
    protected boolean requiresRecalculation() {
        return true;
    }

    @Override
    public List<String> getArguments() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<String> getConditions() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void sample(State state, Random random) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
