/*
 * Copyright (C) 2013 Tim Vaughan <tgvaughan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package plasmidTree.model;

import plasmidTree.CFEventListPlasmid;
import plasmidTree.ConversionEventPlasmid;
import plasmidTree.ConversionPlasmid;
import plasmidTree.ConversionGraphPlasmid;
import plasmidTree.Plasmid;
import beast.core.Description;
import beast.core.Input;
import beast.evolution.tree.Node;
import beast.evolution.tree.Tree;
import beast.evolution.tree.coalescent.PopulationFunction;
import beast.util.Randomizer;
import feast.nexus.NexusBlock;
import feast.nexus.NexusBuilder;
import feast.nexus.TaxaBlock;
import feast.nexus.TreesBlock;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//import java.util.Random;

/**
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
@Description("Simulates an ARG under the full ClonalOrigin model with gain-losses - can be used"
    + " for chain initialization or for sampler validation.")
public class SimulatedACGPlasmid extends ConversionGraphPlasmid {

    public Input<Double> rhoInput = new Input<>(
            "rho",
            "Conversion rate parameter.",
            Input.Validate.REQUIRED);
    
    public Input<Double> lInput = new Input<>(
            "l",
            "Tract length parameter.",
            Input.Validate.REQUIRED);
    
    public Input<PopulationFunction> popFuncInput = new Input<>(
            "populationModel",
            "Demographic model to use.",
            Input.Validate.REQUIRED);

    public Input<Tree> clonalFrameInput = new Input<>(
            "clonalFrame",
            "Optional tree specifying fixed clonal frame.");

    public Input<String> outputFileNameInput = new Input<>(
            "outputFileName",
            "If provided, simulated ARG is additionally written to this file.");

    private double rho, l;
    private PopulationFunction popFunc;
    Map<Plasmid,Map<Node,Boolean>> plasmidPresence;

    public SimulatedACGPlasmid() {
        m_taxonset.setRule(Input.Validate.REQUIRED);
    }
    
    @Override
    public void initAndValidate() {
        
        rho = rhoInput.get();
        l = lInput.get();
        popFunc = popFuncInput.get();
        plasmidPresence = new HashMap<Plasmid,Map<Node,Boolean>>();

        // Need to do this here as Tree.processTraits(), which is called
        // by hasDateTrait() and hence simulateClonalFrame(), expects a
        // tree with nodes.
        super.initAndValidate();
        
        if (clonalFrameInput.get() == null)
            simulateClonalFrame();
        else
            assignFromWithoutID(clonalFrameInput.get());
        
        // Need to do this here as this sets the tree object that the nodes
        // point to, so without it they point to the dummy tree created by
        // super.initAndValidate().
        initArrays();
        
        // Generate recombinations
        generateConversions();
        
        // Write output file
        if (outputFileNameInput.get() != null) {

            NexusBuilder nexusBuilder = new NexusBuilder();
            
            nexusBuilder.append(new TaxaBlock(m_taxonset.get()));
            
            nexusBuilder.append((new TreesBlock() {
                @Override
                public String getTreeString(Tree tree) {
                    return ((ConversionGraphPlasmid)tree).getExtendedNewickPlasmid(plasmidPresence);
                }
            }).addTree(this, "simulatedARG"));
            
            nexusBuilder.append(new NexusBlock() {

                @Override
                public String getBlockName() {
                    return "plasmidTree";
                }

                @Override
                public List<String> getBlockLines() {
                    List<String> lines = new ArrayList<>();

                    String lociLine = "loci";
                    for (Plasmid locus : getLoci())
                        lociLine += " " + locus.getID() + ":" + locus.getSiteCount();
                    lines.add(lociLine);

                    lines.add("clonalframe_labeled " + root.toNewick());
                    lines.add("clonalframe_numbered " + root.toShortNewick(true));
                    for (Plasmid locus : getLoci()) {
                        for (ConversionPlasmid conv : getConversions(locus)) {
                            lines.add("conversion node1=" + conv.getNode1().getNr()
                                    + " node2=" + conv.getNode2().getNr()
                                    //+ " site1=" + conv.getStartSite()
                                    //+ " site2=" + conv.getEndSite()
                                    + " locus=" + locus.getID());
                        }
                    }
                    
                    return lines;
                }
            });

            try (PrintStream pstream = new PrintStream(outputFileNameInput.get())) {
                nexusBuilder.write(pstream);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Use coalescent model to simulate clonal frame.
     */
    private void simulateClonalFrame() {

        // Initialize leaf nodes
        List<Node> leafNodes = new ArrayList<>();
        for (int i=0; i<m_taxonset.get().getTaxonCount(); i++) {
            Node leaf = new Node();
            leaf.setNr(i);
            leaf.setID(m_taxonset.get().getTaxonId(i));
                        
            if (hasDateTrait())
                leaf.setHeight(getDateTrait().getValue(leaf.getID()));
            else
                leaf.setHeight(0.0);
            
            leafNodes.add(leaf);
        }
        
        // Create and sort list of inactive nodes
        List<Node> inactiveNodes = new ArrayList<>(leafNodes);
        Collections.sort(inactiveNodes, (Node n1, Node n2) -> {
            if (n1.getHeight()<n2.getHeight())
                return -1;
            
            if (n1.getHeight()>n2.getHeight())
                return 1;
            
            return 0;
        });
        
        List<Node> activeNodes = new ArrayList<>();
        
        double tau = 0.0;
        int nextNr = leafNodes.size();
        while (true) {
            
            // Calculate coalescence propensity
            int k = activeNodes.size();
            double chi = 0.5*k*(k-1);
            
            // Draw scaled coalescent time
            if (chi>0.0)
                tau += Randomizer.nextExponential(chi);
            else
                tau = Double.POSITIVE_INFINITY;
            
            // Convert to real time
            double t = popFunc.getInverseIntensity(tau);
            
            // If new time takes us past next sample time, insert that sample
            if (!inactiveNodes.isEmpty() && t>inactiveNodes.get(0).getHeight()) {
                Node nextActive = inactiveNodes.remove(0);
                activeNodes.add(nextActive);
                tau = popFunc.getIntensity(nextActive.getHeight());
                continue;
            }
            
            // Coalesce random pair of active nodes.
            Node node1 = activeNodes.remove(Randomizer.nextInt(k));
            Node node2 = activeNodes.remove(Randomizer.nextInt(k-1));
            
            Node parent = new Node();
            parent.addChild(node1);
            parent.addChild(node2);
            parent.setHeight(t);
            parent.setNr(nextNr++);
            
            activeNodes.add(parent);
            
            if (inactiveNodes.isEmpty() && activeNodes.size()<2)
                break;
        }
        
        // Remaining active node is root
        setRoot(activeNodes.get(0));
    }
    
    
    
    private void simulateConversionHistory(List<ConversionPlasmid> conversions, Plasmid locus, Map<Node,List<Double>> lossHistories, Node root) {
    	//System.out.printf("start SCH\n");
    	Boolean topstate = plasmidPresence.get(locus).get(root);
    	for (Node child:root.getChildren()) {
    		//System.out.printf("start of for SCH\n");
    		lossHistories.put(child, new ArrayList<Double>());
    		double time=root.getHeight();
    		double finalTime=child.getHeight();
    		double wait;
    		while (finalTime<time) {
    			if(topstate) wait=Randomizer.nextExponential(l);
    			else wait=Randomizer.nextExponential(rho);
    			if (wait+finalTime>time) {
    				time=finalTime;
    				plasmidPresence.get(locus).put(child,topstate);
    			}else {
    				time=time-wait;
    				lossHistories.get(child).add(time);
    				if (topstate) {
    					topstate=false;
    				}else {
    					ConversionPlasmid conv=new ConversionPlasmid();
    					conv.setLocus(locus);
    					conv.setNode1(child);
    					conv.setHeight1(time);
    					conv.setStartSite(0);
    					conv.setEndSite(locus.getSiteCount()-1);
    					conversions.add(conv);
    					topstate=true;
    				}
    			}
    		}
    		//System.out.printf("pre-print\n");
    		if (lossHistories.get(child)!=null && lossHistories.get(child).size()>1) Collections.reverse(lossHistories.get(child));
    		//System.out.printf("post-print\n");
    		if (!child.isLeaf()) simulateConversionHistory(conversions,locus,lossHistories,child);
    	}
    }
    
    
    //Nicola:simulate gain-loss history and conversion events
    private void generateConversions() {
    	//System.out.printf("beginning of GC\n");
    	for (Plasmid locus : getLoci()) {
    		//System.out.printf("beginning of GC for\n");
    			if (!locus.isPlasmid) continue;
    			//simulate plasmid gains and losses
    			List<ConversionPlasmid> conversions = new ArrayList<ConversionPlasmid>();
    			Map<Node,List<Double>> lossHistories = new HashMap<Node,List<Double>>();
    			Node root=getRoot();
    			lossHistories.put(root, new ArrayList<Double>());
    			plasmidPresence.put(locus, new HashMap<Node,Boolean>());
    			double ran=Randomizer.nextDouble();
    			double lastTime=root.getHeight();
    			System.out.println("ran "+ran+" l "+l+" rho "+rho+" "+l/(l+rho)+" \n");
    			if (ran<l/(l+rho)) {
    				plasmidPresence.get(locus).put(root, false);
    				lastTime=Randomizer.nextExponential(l)+root.getHeight();
    				lossHistories.get(root).add(lastTime);
    				System.out.println("root at height "+root.getHeight()+" is plasmid-absent, its gain at "+lastTime+"\n ");
    			}else {
    				System.out.println("root at height "+root.getHeight()+" is plasmid-present \n ");
    				plasmidPresence.get(locus).put(root, true);
    			}
    			simulateConversionHistory(conversions,locus,lossHistories,root);
    			
    			//System.out.printf("Conversions simulated: \n");
    	        //for (ConversionPlasmid e:conversions) System.out.printf(e.getHeight1()+" "+e.getNode1().getID()+" \n");
    	        //System.out.printf("End Conversions simulated \n");
    			
    			//now simulate the coalescent times of the conversions, accounting for the fact that conversions can only coalesce with CF lineages for which the same plasmid is present
    	        // Create and sort list of inactive nodes
    			List<Node> leafNodes = getExternalNodes();
    	        List<Node> inactiveNodes = new ArrayList<>(leafNodes);
    	        Collections.sort(inactiveNodes, (Node n1, Node n2) -> {
    	            if (n1.getHeight()<n2.getHeight())
    	                return -1;
    	            if (n1.getHeight()>n2.getHeight())
    	                return 1;
    	            return 0;
    	        });
    	        
    	        List<ConversionPlasmid> inactiveConversions = new ArrayList<ConversionPlasmid>(conversions);
    	        Collections.sort(inactiveConversions, (ConversionPlasmid n1, ConversionPlasmid n2) -> {
    	            if (n1.getHeight1()<n2.getHeight1())
    	                return -1;
    	            if (n1.getHeight1()>n2.getHeight1())
    	                return 1;
    	            return 0;
    	        });
    	        
    	        System.out.println("Number of conversions:  "+inactiveConversions.size()+" at times: \n ");
    	        for (ConversionPlasmid conv : inactiveConversions) System.out.println(conv.getHeight1()+", ");
    	        
    	        List<Node> activeNodes = new ArrayList<>();
    			List<Node> coalescableNodes=new ArrayList<Node>();
    			List<ConversionPlasmid> conversions2coal = new ArrayList<ConversionPlasmid>();
    			
    			double tau = 0.0;
    	        while (true) {
    	        	//System.out.printf("start of GC while\n");
    	        	if (conversions2coal.isEmpty() && inactiveConversions.isEmpty())
    	                break;
    	            
    	            // Calculate coalescence propensity
    	        	int c = conversions2coal.size();
    	            int k = coalescableNodes.size();
    	            double chi = c*k;
    	            
    	            // Draw scaled coalescent time
    	            //System.out.printf(chi+" chi\n");
    	            if (chi>0.0) {
    	            	//System.out.printf(tau+" tau\n");
    	                tau += Randomizer.nextExponential(chi);
    	                //System.out.printf(tau+" tau new\n");
    	            }else
    	                tau = Double.POSITIVE_INFINITY;
    	            
    	            // Convert to real time
    	            //System.out.printf(tau+" tau\n");
    	            double t = popFunc.getInverseIntensity(tau);
    	            //System.out.printf(t+" t\n");
    	            
    	            double tauMin=tau, tau1= Double.POSITIVE_INFINITY,tau2= Double.POSITIVE_INFINITY,tau3= Double.POSITIVE_INFINITY,tau4= Double.POSITIVE_INFINITY;
    	            int code=0;
    	            
    	            //System.out.printf("1\n");
    	            
    	            //check next sample's height
    	            if (!inactiveNodes.isEmpty() && t>inactiveNodes.get(0).getHeight()) {
    	            	//System.out.printf(inactiveNodes.get(0).getHeight()+" next sample height\n");
    	            	tau1=popFunc.getIntensity(inactiveNodes.get(0).getHeight());
    	            	code=1;
    	            	//System.out.printf(tau1+" tau1\n");
    	            	tauMin=tau1;
    	            }
    	            //System.out.printf(tauMin+" tauMin\n");
    	            
    	            //System.out.printf("2\n");
    	            
    	            //loop over active nodes' parents
    	            Node firstParent=null;
    	            if (!activeNodes.isEmpty() ) {
    	            	Double minimumParent = Double.POSITIVE_INFINITY;
    	            	if (!activeNodes.get(0).isRoot()) minimumParent=activeNodes.get(0).getParent().getHeight();
    	            	firstParent=activeNodes.get(0);
    	            	for (Node node : activeNodes) {
    	            		if(!node.isRoot() && node.getParent().getHeight()<minimumParent) {
    	            			minimumParent=node.getParent().getHeight();
    	            			firstParent = node;
    	            		}
    	            	}
    	            	if (t>minimumParent) {
    	            		//System.out.printf(minimumParent+" minimumParent\n");
        	            	tau2=popFunc.getIntensity(minimumParent);
        	            	//System.out.printf(tau2+" tau2\n");
        	            }
    	            }
    	            if (tau2<tauMin) {
    	            	tauMin=tau2;
    	            	code=2;
    	            }
    	            //System.out.printf(tauMin+" tauMin\n");
    	            //System.out.printf("3\n");
    	            
    	            //check inactive conversions
    	            if (!inactiveConversions.isEmpty() && t>inactiveConversions.get(0).getHeight1()) {
    	            	//System.out.printf(inactiveConversions.get(0).getHeight1()+" next inactive Conversion\n");
    	            	tau3=popFunc.getIntensity(inactiveConversions.get(0).getHeight1());
    	            	//System.out.printf(tau3+" tau3\n");
    	            }
    	            if (tau3<tauMin) {
    	            	tauMin=tau3;
    	            	code=3;
    	            }
    	            //System.out.printf(tauMin+" tauMin\n");
    	            //System.out.printf("3.1\n");
    	            
    	            //loop over plasmid gains and losses of active nodes.
    	            Node firstLG=null;
    	            if (!activeNodes.isEmpty() ) {
    	            	//System.out.printf("3.2\n");
    	            	Double minimumLG;
    	            	if(!lossHistories.get(activeNodes.get(0)).isEmpty()) {
    	            		//System.out.printf("inside if\n");
    	            		//System.out.printf(activeNodes.get(0).getID()+" node ID\n");
    	            		//System.out.printf(lossHistories.get(activeNodes.get(0)).size()+" loss history size\n");
    	            		minimumLG=lossHistories.get(activeNodes.get(0)).get(0);
    	            	}else {
    	            		minimumLG = Double.POSITIVE_INFINITY;
    	            	}
    	            	//System.out.printf("3.3\n");
    	            	firstLG=activeNodes.get(0);
    	            	for (Node node : activeNodes) {
    	            		//System.out.printf("3.3 cycle\n");
    	            		if(!lossHistories.get(node).isEmpty() && lossHistories.get(node).get(0)<minimumLG) {
    	            			minimumLG=lossHistories.get(node).get(0);
    	            			firstLG = node;
    	            		}
    	            	}
    	            	//System.out.printf("3.4\n");
    	            	if (t>minimumLG) {//inactiveNodes.get(0).getHeight()
    	            		//System.out.printf(minimumLG+" minimumLG\n");
        	            	tau4=popFunc.getIntensity(minimumLG);
        	            	//System.out.printf(tau4+" tau4\n");
        	            }
    	            	//System.out.printf("3.5\n");
    	            }
    	            if (tau4<tauMin) {
    	            	tauMin=tau4;
    	            	code=4;
    	            }
    	            //System.out.printf(tauMin+" tauMin\n");
    	            //System.out.printf(code+" code\n");
    	            
    	            //System.out.printf("4\n");
    	            
    	            //react to the chosen event
    	            tau=tauMin;
    	            t = popFunc.getInverseIntensity(tau);
    	            if (code==0) {
    	            	//System.out.printf(k+"random int 1\n");
    	            	Node node = coalescableNodes.get(Randomizer.nextInt(k));
    	            	//System.out.printf(c+"random int 2\n");
        	            ConversionPlasmid conv = conversions2coal.remove(Randomizer.nextInt(c));
        	            System.out.printf("Conversion from "+conv.getNode1().getID()+" "+conv.getHeight1()+" coalescing at "+node.getID()+" "+t+"\n");
        	            conv.setNode2(node);
        	            conv.setHeight2(t);
        	            //associateConversionWithCF(conv);
            	        addConversion(conv);
    	            } else if (code==1) {
    	            	Node nextActive = inactiveNodes.remove(0);
    	                activeNodes.add(nextActive);
    	                if (plasmidPresence.get(locus).get(nextActive)) coalescableNodes.add(nextActive);
    	            } else if (code==2) {
    	            	Node P=firstParent.getParent();
    	            	activeNodes.add(P);
    	            	if (plasmidPresence.get(locus).get(P)) coalescableNodes.add(P);
    	            	for (Node node :P.getChildren()) {
    	            		activeNodes.remove(node);
    	            		coalescableNodes.remove(node);
    	            	}
    	            } else if (code==3) {
    	            	ConversionPlasmid conv=inactiveConversions.remove(0);
    	            	conversions2coal.add(conv);
    	            } else if (code==4) {
    	            	lossHistories.get(firstLG).remove(0);
    	            	if (coalescableNodes.contains(firstLG)) {
    	            		coalescableNodes.remove(firstLG);
    	            	}else {
    	            		coalescableNodes.add(firstLG);
    	            	}
    	            }
    	             
    	        }
    	        
    	        //System.out.printf("Conversions simulated, top parts: \n");
    	        //for (ConversionPlasmid e:conversions) System.out.printf(e.getHeight2()+" "+e.getNode2().getID()+" \n");
    	        //System.out.printf("End Conversions simulated \n");
 
        }
    }
    
    /**
     * Associates recombination with the clonal frame, selecting points of
     * departure and coalescence.
     * 
     * @param conv recombination to associate
     */
    private void associateConversionWithCF(ConversionPlasmid conv) {
    
        List<CFEventListPlasmid.Event> eventList = getCFEvents();

        // Select departure point            
        double u = Randomizer.nextDouble()*getClonalFrameLength();
        
        boolean started = false;
        for (int eidx=0; eidx<eventList.size(); eidx++) {
            CFEventListPlasmid.Event event = eventList.get(eidx);

            if (!started) {
                
                double interval = eventList.get(eidx+1).getHeight() - event.getHeight();
                
                if (u<interval*event.getLineageCount()) {
                    for (Node node : getNodesAsArray()) {
                        if (!node.isRoot()
                                && node.getHeight()<=event.getHeight()
                                && node.getParent().getHeight()>event.getHeight()) {
                            
                            if (u<interval) {
                                conv.setNode1(node);
                                conv.setHeight1(event.getHeight() + u);
                                break;
                            } else
                                u -= interval;
                            
                        }
                    }
                    started = true;
                    u = Randomizer.nextExponential(1.0);
                } else
                    u -= interval*event.getLineageCount();
            }
            
            if (started) {
                double t = Math.max(event.getHeight(), conv.getHeight1());

                double intervalArea;
                if (eidx<eventList.size()-1) {
                    intervalArea = popFunc.getIntegral(t, eventList.get(eidx+1).getHeight());
                } else
                    intervalArea = Double.POSITIVE_INFINITY;
                
                if (u<intervalArea*event.getLineageCount()) {
                    
                    // Fix height of attachment point

                    double tauEnd = popFunc.getIntensity(t) + u/event.getLineageCount();
                    double tEnd = popFunc.getInverseIntensity(tauEnd);
                    conv.setHeight2(tEnd);                    
                    
                    // Choose particular lineage to attach to
                    int nodeNumber = Randomizer.nextInt(event.getLineageCount());
                    for (Node node : getNodesAsArray()) {
                        if (node.getHeight()<=event.getHeight()
                                && (node.isRoot() || node.getParent().getHeight()>event.getHeight())) {
                            
                            if (nodeNumber == 0) {
                                conv.setNode2(node);
                                break;
                            } else
                                nodeNumber -= 1;
                        }
                    }
                    break;
                } else
                    u -= intervalArea*event.getLineageCount();
            }
        }
    }
}
