/*
 * Copyright (C) 2017 Nicola De Maio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package plasmidTree.model;

//import bacter.AlignmentPlasmid;
//import bacter.ConversionGraph;
import plasmidTree.ConversionGraphPlasmid;
//import bacter.Locus;
//import bacter.MarginalTree;
import plasmidTree.MarginalTreePlasmid;
import plasmidTree.Plasmid;
import plasmidTree.RegionPlasmid;
//import bacter.Region;
import beast.core.Description;
import beast.core.Input;
import beast.evolution.alignment.Alignment;
import beast.evolution.alignment.Sequence;
//import beast.evolution.alignment.Taxon;
import beast.evolution.datatype.DataType;
import beast.evolution.sitemodel.SiteModel;
import beast.evolution.tree.Node;
import beast.util.PackageManager;
import beast.util.Randomizer;
//import beast.util.XMLProducer;
import feast.nexus.CharactersBlock;
import feast.nexus.NexusBuilder;
import feast.nexus.TaxaBlock;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Nicola De Maio
 */
@Description("An alignment produced by simulating sequence evolution down an ACG.")
public class SimulatedAlignmentPlasmid extends Alignment {
    
    public Input<ConversionGraphPlasmid> acgInput = new Input<>(
            "acg",
            "Conversions graph down which to simulate evolution.",
            Input.Validate.REQUIRED);
    
    public Input<SiteModel> siteModelInput = new Input<>(
            "siteModel",
            "site model for leafs in the beast.tree",
            Input.Validate.REQUIRED);
    
    public Input<String> outputFileNameInput = new Input<>(
            "outputFileName",
            "If provided, simulated alignment is additionally written to this file."); 
    
    public Input<Boolean> useNexusInput = new Input<>(
            "useNexus",
            "Use Nexus instead of FASTA format to write alignment file.",
            false);

    public Input<Plasmid> locusInput = new Input<>("locus",
            "Locus for which alignment will be simulated.");
    
    private ConversionGraphPlasmid acg;
    private SiteModel siteModel;
    private DataType dataType;
    private Plasmid locus;
    
    public SimulatedAlignmentPlasmid() {
        sequenceInput.setRule(Input.Validate.OPTIONAL);
    }

    @Override
    public void initAndValidate() {

        acg = acgInput.get();
        siteModel = siteModelInput.get();

        //Plasmid locus;
        if (acg.getLoci().size()==1)
            locus = acg.getLoci().get(0);
        else {
            locus = locusInput.get();
            if (locus == null)
                throw new IllegalArgumentException("Must specify locus" +
                        " when simulating alignment from ACG associated" +
                        " with multiple loci.");
        }

        // We can't wait for Alignment.initAndValidate() to get the
        // data type for us.
        grabDataType();

        // Simulate alignment
        simulate(locus);
        
        //System.out.printf("super.initandvalid\n");
        if(sequenceInput.get().size() != 0) super.initAndValidate();
        
        //System.out.printf("End super.initandvalid\n");
        //List<String> taxa = this.getTaxaNames();
        //for(String s : taxa) System.out.printf(s+" , ");
        //System.out.printf(" End taxa\n");
        
        // Write simulated alignment to disk if requested:
        if (sequenceInput.get().size() != 0 && outputFileNameInput.get() != null) {
            try (PrintStream pstream = new PrintStream(outputFileNameInput.get())) {
                if (useNexusInput.get()) {
                    NexusBuilder nb = new NexusBuilder();
                    nb.append(new TaxaBlock(acg.getTaxonset()));
                    nb.append(new CharactersBlock(this));
                    nb.write(pstream);
                } else {
                    for (String taxonName : acg.getTaxaNames()) {
                        pstream.print(">" + taxonName + "\n");
                        pstream.print(getSequenceAsString(taxonName) + "\n");
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        //System.out.println((((Alignment) this)==null)+" this is a null");
        locus.setAlignment(((Alignment) this));
        if(sequenceInput.get().size() != 0) System.out.printf("End initializing simulated alignment; "+locus.getAlignment().getTaxonCount() +"\n");
        else System.out.printf("End initializing empty alignment; \n");
    }

    /**
     * Perform actual sequence simulation.
     */
    private void simulate(Plasmid locus) {
    	//System.out.printf("Beginning of simulate\n");
        Node cfRoot = acg.getRoot();
        //List<Node> presentNodes= new ArrayList<Node>();
        Map<Node,Boolean> plasmidPresence_old =((SimulatedACGPlasmid)acg).plasmidPresence.get(locus);
        Map<String,Boolean>plasmidPresence = new HashMap<String,Boolean>();
        //System.out.printf("1\n");
        List <Node> leaves = acg.getExternalNodes();
        //System.out.printf("2\n");
        int nTaxa = 0;
        for (Node n : leaves) {
        	if (!locus.isPlasmid) {
        		//if (plasmidPresence_old==null) plasmidPresence = new HashMap<Node,Boolean>();
        		//System.out.printf("2a\n");
        		plasmidPresence.put(n.getID(), true);
        		nTaxa+=1;
        	}else {
        		if (plasmidPresence_old.get(n)) {
        			//System.out.printf("2b\n");
            		plasmidPresence.put(n.getID(), true);
            		nTaxa+=1;
        		}else plasmidPresence.put(n.getID(), false);
        	}
        		
        	//System.out.printf("2c\n");
        }
//        System.out.printf(locus.isPlasmid+" isPlasmid\n");
//        System.out.printf(plasmidPresence.keySet().size()+" plasmidPresence num of keys\n");
//        for (String n:plasmidPresence.keySet()) {
//        	System.out.printf(n+" node ID "+plasmidPresence.get(n)+" is present\n");
//        }
        //System.out.printf("3\n");
        
        int nTaxaTot = acg.getLeafNodeCount();
        //int nTaxa = presentNodes.size();
        
        double[] categoryProbs = siteModel.getCategoryProportions(cfRoot);

        int nCategories = siteModel.getCategoryCount();
        int nStates = dataType.getStateCount();
        double[][] transitionProbs = new double[nCategories][nStates*nStates];
        
        int[][] alignment = new int[nTaxa][locus.getSiteCount()];
        
        //System.out.printf("4\n");
        
        for (RegionPlasmid region : acg.getRegions(locus)) {
        	//System.out.printf("Start for\n");
            int thisLength = region.getRegionLength();

            MarginalTreePlasmid marginalTree = new MarginalTreePlasmid(acg, region);
            
            int[] categories = new int[thisLength];
            for (int i=0; i<thisLength; i++)
                categories[i] = Randomizer.randomChoicePDF(categoryProbs);
            
            int[][] regionAlignment = new int[nTaxaTot][thisLength];
            
            Node thisRoot = marginalTree.getRoot();
            
            int[] parentSequence = new int[region.getRegionLength()];
            double[] frequencies = siteModel.getSubstitutionModel().getFrequencies();
            for (int i=0; i<parentSequence.length; i++)
                parentSequence[i] = Randomizer.randomChoicePDF(frequencies);

            traverse(thisRoot, parentSequence,
                    categories, transitionProbs,
                    regionAlignment, plasmidPresence);
            
            // DEBUG: Count differences
//            int segsites = 0;
//            for (int s=0; s<regionAlignment[0].length; s++) {
//                int state = regionAlignment[0][s];
//                for (int l=1; l<nTaxa; l++) {
//                    if (state != regionAlignment[l][s]) {
//                        segsites += 1;
//                        break;
//                    }
//                }
//            }
            //System.out.println(segsites + " segregating sites in region " + region);
            
            copyToAlignmentPartial(alignment, regionAlignment, region, leaves, plasmidPresence);
        }
        
        //System.out.printf("after for\n");
        
        int presenceIdx=0;
        for (int leafIdx=0; leafIdx<nTaxaTot; leafIdx++) {
        	System.out.printf("Try to put another sequenceInput\n");
        	if(plasmidPresence.get(acg.getNode(leafIdx).getID())) {
        		System.out.printf("Creating sequenceInput for leaf "+acg.getNode(leafIdx).getID()+" "+alignment[presenceIdx].length+"\n");
        		String sSeq = dataType.state2string(alignment[presenceIdx]);
                String sTaxon = acg.getNode(leafIdx).getID();
                sequenceInput.setValue(new Sequence(sTaxon, sSeq), this);
        		presenceIdx+=1;
        		//System.out.printf("End if\n");
        	}
        	//System.out.printf("End another for\n");
        }
    }
    
    /**
     * Traverse a marginal tree simulating a region of the sequence alignment
     * down it.
     * 
     * @param node Node of the marginal tree
     * @param parentSequence Sequence at the parent node in the marginal tree
     * @param categories Mapping from sites to categories
     * @param transitionProbs
     * @param regionAlignment 
     */
    private void traverse(Node node,
            int[] parentSequence,
            int[] categories, double[][] transitionProbs,
            int[][] regionAlignment, Map<String,Boolean> plasmidPresence) {
    	//System.out.printf("Traverse\n");
    	//System.out.printf(node.getID()+" "+node.getHeight()+" parent node\n");
        for (Node child : node.getChildren()) {
        	//System.out.printf(child.getID()+" "+child.getHeight()+" New child node\n");
            // Calculate transition probabilities
            for (int i=0; i<siteModel.getCategoryCount(); i++) {
                siteModel.getSubstitutionModel().getTransitionProbabilities(
                        child, node.getHeight(), child.getHeight(),
                        siteModel.getRateForCategory(i, child),
                        transitionProbs[i]);
                //System.out.printf(node.getHeight()+" "+child.getHeight()+" "+siteModel.getRateForCategory(i, child)+" \n");
                //for(int j=0;j<transitionProbs[i].length;j+=1)System.out.printf(transitionProbs[i][j]+" ");
            }
            //System.out.printf("C1\n");
            // Draw characters on child sequence
            int[] childSequence = new int[parentSequence.length];
            int nStates = dataType.getStateCount();
            double[] charProb = new double[nStates];
            for (int i=0; i<childSequence.length; i++) {
                int category = categories[i];
                System.arraycopy(transitionProbs[category],
                        parentSequence[i]*nStates, charProb, 0, nStates);
                //System.out.printf(charProb[0]+" "+charProb[1]+" "+charProb[2]+" "+charProb[3]+" \n");
                childSequence[i] = Randomizer.randomChoicePDF(charProb);
            }
            //System.out.printf("C2\n");
            //System.out.printf(child.getID()+" child.getID "+child.getHeight()+" height "+child+"\n");
            if (child.isLeaf()) {
            	//System.out.printf("C2a\n");
            	//System.out.printf(child.getID()+" leafID\n");
            	//System.out.printf(plasmidPresence.keySet().size()+" plasmidPresence num of keys\n");
//                for (String n:plasmidPresence.keySet()) {
//                	System.out.printf(n+" node ID "+plasmidPresence.get(n)+" is present\n");
//                	//System.out.printf(n.getHeight()+" height "+n+"\n");
//                }
//                System.out.printf("Plasmidpresence of child "+plasmidPresence.get(child.getID())+"\n");
            	if (plasmidPresence.get(child.getID())){ // .hasPlasmid(child.getNr()))
            		//System.out.printf("C2a1\n");
            		System.arraycopy(childSequence, 0, regionAlignment[child.getNr()], 0, childSequence.length);
            		System.out.printf("Child "+child.getID()+" is present, sequence "+childSequence.length+ "\n");
            	}else{
            		//System.out.printf("C2a3\n");
            		regionAlignment[child.getNr()]=null;
            		System.out.printf("Child "+child.getID()+" is not present\n");
            		//System.out.printf("C2a4\n");
//            		for(int i=0; i<childSequence.length; i++){
//            			regionAlignment[child.getNr()][i]=-1;
//            		}
            	}
            	//System.out.printf("C2b\n");
                //System.arraycopy(childSequence, 0, regionAlignment[child.getNr()], 0, childSequence.length);
            } else {
            	//System.out.printf("C2c\n");
                traverse(child, childSequence,
                        categories, transitionProbs,
                        regionAlignment, plasmidPresence);
                //System.out.printf("C2d\n");
            }
            //System.out.printf("C3\n");
        }
    }
    
    
    
    /**
     * Copy region alignment over to plasmid alignment.
     *
     * @param alignment
     * @param regionAlignment
     * @param region
     */
    private void copyToAlignmentPartial(int[][] alignment, int[][] regionAlignment,
            RegionPlasmid region, List<Node> leaves, Map<String,Boolean> plasmidPresence) {
        int nTaxa = regionAlignment.length;
        int presenceIdx=0;
        for (int leafIdx=0; leafIdx<nTaxa; leafIdx++) {
        	if (plasmidPresence.get(leaves.get(leafIdx).getID())) {
        		System.arraycopy(regionAlignment[leafIdx], 0, alignment[presenceIdx], 0, region.getRegionLength());
        		presenceIdx+=1;
        	}
        }
        System.out.printf("copyToAlignmentPartial, nTaxa: "+nTaxa+" regionAlignment.length "+regionAlignment.length+" alignment.length "+alignment.length+"\n");
    }
    
    /**
     * Copy region alignment over to full alignment.
     *
     * @param alignment
     * @param regionAlignment
     * @param region
     */
    private void copyToAlignment(int[][] alignment, int[][] regionAlignment,
            RegionPlasmid region) {
        
        int nTaxa = alignment.length;
        
        for (int leafIdx=0; leafIdx<nTaxa; leafIdx++) {
            System.arraycopy(regionAlignment[leafIdx], 0,
                    alignment[leafIdx], 0,
                    region.getRegionLength());
        }
    }
    
    /**
     * HORRIBLE function to identify data type from given description.
     * 
     * @return DataType instance (null if none found)
     */
    private void grabDataType() {
        if (userDataTypeInput.get() != null) {
            dataType = userDataTypeInput.get();
        } else {

            List<String> dataTypeDescList = new ArrayList<>();
            List<String> classNames = PackageManager.find(beast.evolution.datatype.DataType.class, "beast.evolution.datatype");
            for (String className : classNames) {
                try {
                    DataType thisDataType = (DataType) Class.forName(className).newInstance();
                    if (dataTypeInput.get().equals(thisDataType.getTypeDescription())) {
                        dataType = thisDataType;
                        break;
                    }
                    dataTypeDescList.add(thisDataType.getTypeDescription());
                } catch (ClassNotFoundException
                    | InstantiationException
                    | IllegalAccessException e) {
                }
            }
            if (dataType == null) {
                throw new IllegalArgumentException("Data type + '"
                        + dataTypeInput.get()
                        + "' cannot be found.  Choose one of "
                        + Arrays.toString(dataTypeDescList.toArray(new String[0])));
            }
        }
    }
}
