/*
 * Copyright (C) 2014 Tim Vaughan <tgvaughan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package plasmidTree;

import com.google.common.collect.Sets;




import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class is used to maintain a list of marginal tree regions
 * corresponding to a given ACG.
 *
 * @author Tim Vaughan <tgvaughan@gmail.com>
 */
public class RegionListPlasmid {

    private final List<RegionPlasmid> regions;
    private Plasmid locus;
    private boolean dirty;

    /**
     * Ancestral conversion graph this list belongs to.
     */
    private final ConversionGraphPlasmid acg;

    /**
     * Construct a new region list for the given ACG.  There should only
     * be one of these objects per ACG object, created during the ACG
     * initAndValidate().
     *
     * @param acg Conversion graph from which to compute region list.
     * @param locus Locus with which this region list is associated.
     */
    public RegionListPlasmid(ConversionGraphPlasmid acg, Plasmid locus) {
        this.acg = acg;
        this.locus = locus;
        regions = new ArrayList<>();
        dirty = true;
    }

    /**
     * Obtain list of contiguous regions having fixed marginal trees.
     * 
     * @return region list
     */
    public List<RegionPlasmid> getRegions() {
        updateRegionList();

        return regions;
    }

    /**
     * Retrieve number of regions in region list.
     * 
     * @return region count
     */
    public int getRegionCount() {
        updateRegionList();

        return regions.size();
    }

    /**
     * Mark the region list as dirty.
     */
    public void makeDirty() {
        dirty = true;
    }
   
    /**
     * Assemble list of regions of contiguous sites that possess a single
     * marginal tree.
     */
    public void updateRegionList() {
        if (!dirty)
            return;

        regions.clear();

        AffectedSiteListPlasmid affectedSiteList = new AffectedSiteListPlasmid(acg);

        /* Assemble lists of conversions ordered by start and end sites.
        Note that these are COPIES of the conversion objects attached
        to the ACG. This ensures that subsequent modifications of these
        objects won't break our contract with the HashSet<Conversion>
        objects in the likelihood code.
        */
        List<ConversionPlasmid> convOrderedByStart = new ArrayList<>();
        acg.getConversions(locus).forEach(conversion -> {
            if (affectedSiteList.affectedSiteCount.get(conversion)>0)
                convOrderedByStart.add(conversion.getCopy());
        });
        convOrderedByStart.sort((ConversionPlasmid o1, ConversionPlasmid o2) -> o1.getStartSite() - o2.getStartSite());

        List<ConversionPlasmid> convOrderedByEnd = new ArrayList<>();
        convOrderedByEnd.addAll(convOrderedByStart);
        convOrderedByEnd.sort((ConversionPlasmid o1, ConversionPlasmid o2) -> o1.getEndSite() - o2.getEndSite());

        Set<ConversionPlasmid> activeConversions = Sets.newHashSet();

        int lastBoundary = 0;

        while (!convOrderedByStart.isEmpty() || !convOrderedByEnd.isEmpty()) {

            int nextStart;
            if (!convOrderedByStart.isEmpty())
                nextStart = convOrderedByStart.get(0).getStartSite();
            else
                nextStart = Integer.MAX_VALUE;

            int nextEnd;
            if (!convOrderedByEnd.isEmpty())
                nextEnd = convOrderedByEnd.get(0).getEndSite() + 1;
            else
                nextEnd = Integer.MAX_VALUE;

            int nextBoundary = Math.min(nextStart, nextEnd);
            if (nextBoundary > lastBoundary) {
                RegionPlasmid region = new RegionPlasmid(lastBoundary, nextBoundary, activeConversions);
                regions.add(region);
            }

            if (nextStart < nextEnd) {
                activeConversions.add(convOrderedByStart.get(0));
                convOrderedByStart.remove(0);
                lastBoundary = nextStart;
            } else {
                activeConversions.remove(convOrderedByEnd.get(0));
                convOrderedByEnd.remove(0);
                lastBoundary = nextEnd;
            }
        }

        if (lastBoundary < locus.getSiteCount()) {
            RegionPlasmid region = new RegionPlasmid(lastBoundary, locus.getSiteCount(), new HashSet<>());
            regions.add(region);
        }

        dirty = false;
    }
}
