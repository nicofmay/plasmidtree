/*
 * Copyright (C) 2018 Nicola De Maio <nicola.de.maio.85@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package plasmidTree;

import bacter.Locus;
import beast.core.Description;
import beast.core.Input;
import beast.evolution.alignment.Alignment;
import beast.evolution.alignment.TaxonSet;

/**
 * @author Nicola De Maio <nicola.de.maio.85@gmail.com>
 */
@Description("A locus of a given length with which an ACG can be associated.")
public class Plasmid extends Locus {
	
	public Input<Boolean> isPlasmidInput = new Input<>(
            "isPlasmid",
            "is this a plasmid (true), or the chromosome (false) ? default is plasmid.", true);
	
	public Input<TaxonSet> taxonSetInput = new Input<>(
            "taxonSet",
            "Set of taxa.");
	
	public Boolean isPlasmid;
	
	public TaxonSet taxonSet; 
	
    public Plasmid() { }
    
//    @Override
//    public void initAndValidate() {
//        if (alignmentInput.get() != null) {
//            alignment = alignmentInput.get();
//            siteCount = alignment.getSiteCount();
//            taxonSet = new TaxonSet(alignment);
//        } else {
//            alignment = null;
//            siteCount = siteCountInput.get();
//            taxonSet = taxonSetInput.get();
//        }
//        isPlasmid = isPlasmidInput.get();
//    }

    /**
     * Construct locus corresponding to given alignment.
     *
     * @param alignment alignment object
     */
    public Plasmid(String name, Alignment alignment, Boolean isPlasmid) {
        this.alignment = alignment;
        this.siteCount = alignment.getSiteCount();
        this.isPlasmid = isPlasmid;
        setID(name);
    }

    /**
     * Construct locus with given site count
     *
     * @param siteCount length of this locus
     */
    public Plasmid(String name, int siteCount, Boolean isPlasmid) {
        this.alignment = null;
        this.siteCount = siteCount;
        this.isPlasmid = isPlasmid;
        setID(name);
    }

    @Override
    public void initAndValidate() {
    		super.initAndValidate();
        isPlasmid = isPlasmidInput.get();
    }
    
    public void setAlignment(Alignment a) {
    	alignment=a;
    }

}
