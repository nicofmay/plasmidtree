plasmidTree
======

plasmidTree is a [BEAST 2](http://www.beast2.org) package, extension of Tim Vaughan’s [bacter](https://tgvaughan.github.io/bacter/) package, which infers plasmid evolution aided by the joint modelling of a bacterial clonal frame and plasmid tree and gains and losses.
It allows to infer the clonal frame, plasmid trees, and plasmid gain and loss rates from a sequence alignment. The chromosomal alignment is assumed to not contain recombination. The model is an adaptation of the model described in [Didelot et al.'s 2010 Genetics paper][1].


Building package from source
----------------------------

To build this package from source, ensure you have the following installed:

* Java JDK v1.8 
* Apache Ant v1.9 or later
* An internet connection

The internet connection is required since the build script downloads the most
recent version of the BEAST 2 source to build the package against.
Assuming both Java and Ant are on your execution path and your CWD is the root of
this archive, simply type "ant" from the command line to build the package.
This may take up to a minute due to the script fetching the BEAST source, and
the resulting binary will be left in the `/dist` directory.
To run the unit tests, use "ant test".

License
-------

This software is free (as in freedom). You are welcome to use it, modify it,
and distribute your modified versions provided you extend the same courtesy to
users of your modified version.  Specifically, it is made available under the
terms of the GNU General Public License version 3, which is contained in his
directory in the file named COPYING.


[1]: http://www.genetics.org/content/186/4/1435
[2]: http://nbviewer.jupyter.org/github/tgvaughan/bacter/blob/master/examples/Validation.ipynb
